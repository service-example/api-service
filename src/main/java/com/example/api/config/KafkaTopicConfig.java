package com.example.api.config;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@RequiredArgsConstructor
public class KafkaTopicConfig {

    @Bean
    public NewTopic personTopic() {
        return TopicBuilder.name("person").build();
    }

    @Bean
    public NewTopic fileTopic() {
        return TopicBuilder.name("file").build();
    }
}