package com.example.api.config;

import com.example.api.connector.AllTopicsWebSocketConnector;
import com.example.api.connector.TopicWebSocketConnector;
import com.example.common.config.ConfigProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.kafka.receiver.ReceiverOptions;

import java.util.*;
import java.util.regex.Pattern;

@Slf4j
@EnableWebFlux
@Configuration
public class WebSocketConfig {

    private final List<String> topics = Arrays.asList("file", "person");

    private final ConfigProperties config;

    private final Map<String, Object> consumerConfig;

    public WebSocketConfig(ConfigProperties config) {
        this.config = config;
        this.consumerConfig = new HashMap<>();
        this.consumerConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, config.getKafkaBootstrapServers());
        this.consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        this.consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    }

    private AllTopicsWebSocketConnector allTopicsReceiverOptions() {
        return new AllTopicsWebSocketConnector(
                ReceiverOptions
                        .<String, String>create(consumerConfig)
                        .consumerProperty(ConsumerConfig.GROUP_ID_CONFIG, "gateway-group")
                        .subscription(Arrays.asList("file", "person")));
    }

    private TopicWebSocketConnector topicWebSocketConnector(String topic) {
        return new TopicWebSocketConnector(
                ReceiverOptions
                        .<String, String>create(consumerConfig)
                        .consumerProperty(ConsumerConfig.GROUP_ID_CONFIG, topic + "-group")
                        .subscription(Collections.singleton(topic)));
    }

    @Bean
    public HandlerMapping webSocketHandlerMapping() {
        Map<String, WebSocketHandler> handler = new HashMap<>();

        handler.put("/topics", allTopicsReceiverOptions());
        for (String topic : topics) handler.put("/topics/" + topic, topicWebSocketConnector(topic));

        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setOrder(Ordered.HIGHEST_PRECEDENCE);
        handlerMapping.setUrlMap(handler);
        return handlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }
}
