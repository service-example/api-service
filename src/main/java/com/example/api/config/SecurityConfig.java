package com.example.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        return http // @formatter:off
                .csrf()
                    .disable()
                .authorizeExchange()
                    .pathMatchers("/actuator/**").permitAll()
                    .and()
                .authorizeExchange()
                    .pathMatchers("/**").permitAll()
                    .and()
                .authorizeExchange()
                    .pathMatchers("/api/**").authenticated()
                    .and()
                .httpBasic()
                    .disable()
                .oauth2ResourceServer()
                    .jwt()
                        .and()
                    .and()
                .build(); // @formatter:on
    }

}