package com.example.api.connector;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;

@RequiredArgsConstructor
public class TopicWebSocketConnector implements WebSocketHandler {

    private final ReceiverOptions<String, String> receiverOptions;

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        return session
                .send(KafkaReceiver
                        .create(receiverOptions)
                        .receive()
                        .map(ConsumerRecord::value)
                        .log("[Topic received]")
                        .map(session::textMessage));
    }
}
